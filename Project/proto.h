#include <windows.h>
#include <windowsx.h>
#include <commctrl.h>
#include <string.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include "project_v2_0res.h"

#define SEARCH_ID			7000
#define MASK_ID_SEARCH		7001
#define TRAGZAHL_ID			7002
#define MASK_ID_TRAGZAHL	7003
#define LIFE_ID				7004
#define MASK_ID_LIFE		7005
#define UMDREHUNG_ID		7006
#define MASK_ID_UMDREHUNG	7007

#define SEARCHBAR			8000
#define RESULT				8001
#define FA_TEXT				8002
#define C0_TEXT				8003
#define FR_TEXT				8004
#define X_TEXT				8005
#define Y_TEXT				8006
#define N_TEXT				8007
#define C_TEXT				8008

#define N1_TEXT				8011
#define N2_TEXT				8012
#define N3_TEXT				8013
#define N4_TEXT				8014
#define N5_TEXT				8015

#define P1_TEXT				8021
#define P2_TEXT				8022
#define P3_TEXT				8023
#define P4_TEXT				8024
#define P5_TEXT				8025

#define B_RESULT			9000
#define B_EXIT				9001
#define B_NEXT				9002

#define L_SEARCHBAR			10000
#define L_RESULT			10001
#define L_DEF				10002
#define L_EINHEIT			10003
#define L_WARNING			10004

#define L_FR				10011
#define L_FA				10012
#define L_X					10013
#define L_Y					10014
#define L_C					10015
#define L_N					10016

#define BOX_1				7550
#define	BOX_2				7551

#define RB_1				7501
#define RB_2				7502
#define RB_3				7503
#define RB_4				7504
#define RB_5				7505
#define RB_6				7506
#define RB_7				7507
#define RB_8				7508


#define Kugellager_Tabelle      "Kugellager.csv"
#define Rollenlager_Tabelle     "Rollenlager.csv"
#define Tragzahl_Tabelle		"Tragzahl.csv"

FILE* file1;
FILE* file2;
FILE* file3;

int found;
char buffer [50];
double nm;
int radio_1;
int radio_2;
int radio_3;
int radio_4;
int radio_5;
int radio_6;
int radio_7;
int radio_8;

struct Tragzahl {

char Fa_C0 [6];
char e [5];
char X [5];
char Y [5];

};

struct Lager {

char Kurzzeichen[10];
char d[10];
char D[10];
char B[10];
char C[10];
char C_0[10];

};

struct Umdrehungen {

	char n[10];
	char prz[10];
};

struct Lager Result_Lager[46];

struct Tragzahl Result_Tragzahl;

struct Umdrehungen Result_Umdrehungen[5];


FILE* file_open(char file_name[20], HWND hwnd);

void interpolation(FILE* new_file, double input, char F_a_char[10],char F_r_char[10], char C_0_char[10]);

void search(FILE* new_file, char phrase[25], int spalte_search);

int get_criteria(HWND hwnd);

int get_criteria_life(HWND hwnd);

void lifetime(double result[3],int size_t, double input[size_t], double p);

double medium(double result[2][5]);

BOOL APIENTRY maskenprogramm(HWND hwnd,UINT msg,WPARAM wParam,LPARAM lParam);

BOOL WINAPI HandleDefaultMessages(HWND hwnd,UINT msg,WPARAM wParam,LPARAM lParam);

BOOL APIENTRY tragzahlprogramm(HWND hwnd, UINT msg,WPARAM wParam,LPARAM lParam);

BOOL APIENTRY lifeprogramm(HWND hwnd,UINT msg,WPARAM wParam,LPARAM lParam);

BOOL APIENTRY umdrehungen(HWND hwnd,UINT msg,WPARAM wParam,LPARAM lParam);
