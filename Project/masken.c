#include "proto.h"

BOOL APIENTRY maskenprogramm(HWND hwnd, UINT msg,WPARAM wParam,LPARAM lParam){


	char searchbar[25];
	int column;
	int check =1;
	char temp[500];
	char temp1[50];
	sprintf(temp,"\0");

	switch (msg){

	case WM_INITDIALOG:

		CheckDlgButton(hwnd,RB_1,BST_CHECKED);
		CheckDlgButton(hwnd,RB_7,BST_CHECKED);

	case WM_COMMAND:

		switch (LOWORD(wParam)){


		case B_RESULT:

			radio_1,radio_2,radio_3,radio_4,radio_5,radio_6,radio_7,radio_8 =0;

			column = get_criteria(hwnd);

			radio_7 = IsDlgButtonChecked(hwnd,RB_7);
			radio_8 = IsDlgButtonChecked(hwnd,RB_8);

			if(radio_7 == 1){

				Edit_GetText(GetDlgItem(hwnd,SEARCHBAR),searchbar,25);
				file1 = file_open(Kugellager_Tabelle,hwnd);
				search(file1,searchbar,column);

				for(int i=0;i<found;i++){
					sprintf(temp1,"%s | %s | %s | %s | %s | %s\r\n",Result_Lager[i].Kurzzeichen,Result_Lager[i].d,Result_Lager[i].D,Result_Lager[i].B,Result_Lager[i].C,Result_Lager[i].C_0);
					strcat(temp,temp1);
				}

				if(found == 0){
					Edit_SetText(GetDlgItem(hwnd,RESULT),"nichts gefunden");
				}
				else{
					Edit_SetText(GetDlgItem(hwnd,RESULT),temp);
				}
				break;
			}
			if(radio_8==1){

				Edit_GetText(GetDlgItem(hwnd,SEARCHBAR),searchbar,25);
				file2 = file_open(Rollenlager_Tabelle,hwnd);
				search(file2,searchbar,column);

				for(int i=0;i<found;i++){
					sprintf(temp1,"%s | %s | %s | %s | %s | %s\r\n",Result_Lager[i].Kurzzeichen,Result_Lager[i].d,Result_Lager[i].D,Result_Lager[i].B,Result_Lager[i].C,Result_Lager[i].C_0);
					strcat(temp,temp1);
				}

				if(found == 0){
					Edit_SetText(GetDlgItem(hwnd,RESULT),"nichts gefunden");
				}
				else{
					Edit_SetText(GetDlgItem(hwnd,RESULT),temp);
				}
				break;
			}

		case B_EXIT:
			PostMessage(hwnd,WM_CLOSE,0,0);
		}


	}
	return(HandleDefaultMessages(hwnd,msg,wParam,lParam));

}

BOOL APIENTRY tragzahlprogramm(HWND hwnd, UINT msg,WPARAM wParam,LPARAM lParam){

	char F_a [10];
	char F_r [10];
	char C_0 [10];
	char temp[100];
	double input;

	switch(msg){

	case WM_COMMAND:

		switch(LOWORD(wParam)){

		case B_RESULT:
			file3=file_open(Tragzahl_Tabelle,hwnd);

			Edit_GetText(GetDlgItem(hwnd,FA_TEXT),F_a,10);
			Edit_GetText(GetDlgItem(hwnd,FR_TEXT),F_r,10);
			Edit_GetText(GetDlgItem(hwnd,C0_TEXT),C_0,10);

			double input = atof(F_a)/atof(C_0);

			interpolation(file3,input,F_a,F_r,C_0);

			sprintf(temp,"e = %s | X = %s | Y = %s",Result_Tragzahl.e,Result_Tragzahl.X,Result_Tragzahl.Y);
			Edit_SetText(GetDlgItem(hwnd,RESULT),temp);

			break;

		case B_EXIT:
			PostMessage(hwnd,WM_CLOSE,0,0);
		}
	}
	return(HandleDefaultMessages(hwnd,msg,wParam,lParam));
}

BOOL APIENTRY lifeprogramm(HWND hwnd,UINT msg,WPARAM wParam,LPARAM lParam){

	char data_input[6][10];
	double temp[3];
	double input[6];
	int Lager_art;
	int size_t = sizeof(input)/sizeof(double);
	double p;
	char text[1000];
	sprintf(text,"\0");

	switch(msg){

	case WM_INITDIALOG:


		CheckDlgButton(hwnd,RB_7,BST_CHECKED);


	case WM_COMMAND:

		switch(LOWORD(wParam)){

		case B_NEXT:

			HINSTANCE hInst = GetModuleHandle(NULL);
			DialogBox(hInst,MAKEINTRESOURCE(MASK_ID_UMDREHUNG),hwnd,&umdrehungen);

			break;

		case B_RESULT:

			radio_7, radio_8=0;

			Lager_art = get_criteria_life(hwnd);

			Edit_GetText(GetDlgItem(hwnd,FA_TEXT),data_input[0],10);
			Edit_GetText(GetDlgItem(hwnd,FR_TEXT),data_input[1],10);
			Edit_GetText(GetDlgItem(hwnd,C_TEXT),data_input[2],10);
			Edit_GetText(GetDlgItem(hwnd,X_TEXT),data_input[3],10);
			Edit_GetText(GetDlgItem(hwnd,Y_TEXT),data_input[4],10);
			Edit_GetText(GetDlgItem(hwnd,N_TEXT),data_input[5],10);

			input[0] = atof(data_input[1]);
			input[1] = atof(data_input[3]);
			input[2] = atof(data_input[0]);
			input[3] = atof(data_input[4]);
			input[4] = atof(data_input[2]);
			input[5] = atof(data_input[5]);

			if(Lager_art == 1){

				p=3;
				lifetime(temp, size_t, input,p);
				sprintf(text,"Die Lebenszeit des Kugellagers mit 90%% Wahrscheinlichkeit\r\nund unter normalen Einsatzbedingungen:\r\n1. Betriebs Lebensdauer L_10h:		%.1lf Stunden\r\n2. Nominelle Lebensdauer L_10:		%.1lf Stunden\r\n3. Dynamische �quivalente Lagerlast P:	%.1lf kN",temp[2],temp[1],temp[0]);
				Edit_SetText(GetDlgItem(hwnd,RESULT),text);
				printf("%s\n",data_input[1]);
				free(data_input[1]);
				printf("%s\n",data_input[1]);
			}
			if(Lager_art == 2){

				p=10/3;
				input[1]=1;
				input[3]=0;
				lifetime(temp, size_t, input,p);
			}
			break;

		case B_EXIT:
			PostMessage(hwnd,WM_CLOSE,0,0);
		}
	}

	return(HandleDefaultMessages(hwnd,msg,wParam,lParam));
}

BOOL APIENTRY umdrehungen(HWND hwnd,UINT msg,WPARAM wParam,LPARAM lParam){


	double data_input[2][5];
	nm=0;

	switch(msg){
	case WM_COMMAND:

		switch(LOWORD(wParam)){

		case B_RESULT:

			Edit_GetText(GetDlgItem(hwnd,N1_TEXT),Result_Umdrehungen[0].n,10);
			Edit_GetText(GetDlgItem(hwnd,N2_TEXT),Result_Umdrehungen[1].n,10);
			Edit_GetText(GetDlgItem(hwnd,N3_TEXT),Result_Umdrehungen[2].n,10);
			Edit_GetText(GetDlgItem(hwnd,N4_TEXT),Result_Umdrehungen[3].n,10);
			Edit_GetText(GetDlgItem(hwnd,N5_TEXT),Result_Umdrehungen[4].n,10);

			Edit_GetText(GetDlgItem(hwnd,P1_TEXT),Result_Umdrehungen[0].prz,10);
			Edit_GetText(GetDlgItem(hwnd,P2_TEXT),Result_Umdrehungen[1].prz,10);
			Edit_GetText(GetDlgItem(hwnd,P3_TEXT),Result_Umdrehungen[2].prz,10);
			Edit_GetText(GetDlgItem(hwnd,P4_TEXT),Result_Umdrehungen[3].prz,10);
			Edit_GetText(GetDlgItem(hwnd,P5_TEXT),Result_Umdrehungen[4].prz,10);


			for(int i=0; i<=4;i++){
				data_input[0][i]=atof(Result_Umdrehungen[i].n);
				//printf("%.3lf	",data_input[0][i]);
				data_input[1][i]=atof(Result_Umdrehungen[i].prz);
				//printf("%.3lf\n",data_input[1][i]);
			}

			nm = medium(data_input);

			printf("%3.lf",nm);
			break;

		case B_EXIT:
			PostMessage(hwnd,WM_CLOSE,0,0);
			break;

		}
		break;
	}

	return(HandleDefaultMessages(hwnd,msg,wParam,lParam));

}
