#include "proto.h"

FILE* file_open(char file_name[20], HWND hwnd){

	FILE* new_file= fopen(file_name,"r");

	if(new_file == NULL){
		MessageBox(hwnd,"Datei konnte nicht ge�ffnet werden!","Fehler",MB_OK | MB_ICONEXCLAMATION);
		exit(1);
	}
	return new_file;
}

int get_criteria(HWND hwnd){


	int column = 0;

	radio_1 = IsDlgButtonChecked(hwnd,RB_1);
	radio_2 = IsDlgButtonChecked(hwnd,RB_2);
	radio_3 = IsDlgButtonChecked(hwnd,RB_3);
	radio_4 = IsDlgButtonChecked(hwnd,RB_4);
	radio_5 = IsDlgButtonChecked(hwnd,RB_5);
	radio_6 = IsDlgButtonChecked(hwnd,RB_6);

	while(column == 0){
		if(radio_1 == 1){
			column =1;
			break;
		}
		if(radio_2 == 1){
			column =2;
			break;
		}
		if(radio_3 == 1){
			column =3;
			break;
		}
		if(radio_4 == 1){
			column =4;
			break;
		}
		if(radio_5 == 1){
			column =5;
			break;
		}
		if(radio_6 == 1){
			column =6;
			break;
		}
	}
	return column;
}

void interpolation(FILE* new_file, double input, char F_a_char[10], char F_r_char[10], char C_0_char[10]){

	struct Tragzahl Tabelle[6];

	double F_a = atof(F_a_char);
	double F_r = atof(F_r_char);
	double C_0 = atof(C_0_char);
	int j = 0;

	fgets(buffer,sizeof(buffer),new_file);

	while(fgets(buffer,sizeof(buffer),new_file)){

		char* token=strtok(buffer,";");

		for(int i=1;i<= 4;i++){

			switch(i){

			case 1:

				sprintf(Tabelle[j].Fa_C0,token);
				token=strtok(NULL,";");
				break;
			case 2:

				sprintf(Tabelle[j].e,token);
				token=strtok(NULL,";");
				break;
			case 3:

				sprintf(Tabelle[j].X,token);
				token=strtok(NULL,";");
				break;
			case 4:

				sprintf(Tabelle[j].Y,token);
				token=strtok(NULL,";");
				break;
			}
		}
		j++;
	}

	double Fa_C0 = F_a/C_0;
	double Fa_Fr = F_a/F_r;

	for(int i=0;i<=5;i++){

		if(Fa_C0 >= atof(Tabelle[i].Fa_C0) && Fa_C0 <= atof(Tabelle[i+1].Fa_C0)){

			if((Fa_C0-atof(Tabelle[i].Fa_C0)) < (atof(Tabelle[i+1].Fa_C0)-Fa_C0)){

				if(Fa_Fr > atof(Tabelle[i].e)){
					printf("e =%s\n",Tabelle[i].e);
					printf("X=%s	Y=%s", Tabelle[i].X,Tabelle[i].Y);
					sprintf(Result_Tragzahl.e,Tabelle[i].e);
					sprintf(Result_Tragzahl.X,Tabelle[i].X);
					sprintf(Result_Tragzahl.Y,Tabelle[i].Y);
					break;
				}
				else{
					printf("X=1	Y=0");
					sprintf(Result_Tragzahl.e,"keins");
					sprintf(Result_Tragzahl.X,"1");
					sprintf(Result_Tragzahl.Y,"0");
					break;
				}
			}
			else{
				if(Fa_Fr > atof(Tabelle[i+1].e)){
					printf("e =%s\n",Tabelle[i+1].e);
					printf("X=%s	Y=%s", Tabelle[i].X,Tabelle[i+1].Y);
					sprintf(Result_Tragzahl.e,Tabelle[i+1].e);
					sprintf(Result_Tragzahl.X,Tabelle[i+1].X);
					sprintf(Result_Tragzahl.Y,Tabelle[i+1].Y);
					break;
				}
				else{
					printf("X=1	Y=0");
					sprintf(Result_Tragzahl.e,"keins");
					sprintf(Result_Tragzahl.X,"1");
					sprintf(Result_Tragzahl.Y,"0");

					break;
				}
			}
		}
	}
	fclose(new_file);
}


void search(FILE* new_file,char phrase [25], int spalte_search){

	struct Lager Tabelle[45];

	int j = 0;
	int not_found= 1;
	found = 0;

	fgets(buffer,sizeof(buffer),new_file);

	while(fgets(buffer,sizeof(buffer),new_file)){

		char* token=strtok(buffer,";");

		for(int i=1;i<= 6;i++){

			switch(i){

			case 1:
				sprintf(Tabelle[j].Kurzzeichen,token);
				token=strtok(NULL,";");
				break;
			case 2:
				sprintf(Tabelle[j].d,token);
				token=strtok(NULL,";");
				break;
			case 3:
				sprintf(Tabelle[j].D,token);
				token=strtok(NULL,";");
				break;
			case 4:
				sprintf(Tabelle[j].B,token);
				token=strtok(NULL,";");
				break;
			case 5:
				sprintf(Tabelle[j].C,token);
				token=strtok(NULL,";");
				break;
			case 6:
				sprintf(Tabelle[j].C_0,token);
				token=strtok(NULL,";");
				break;
			}
		}
		j++;
	}

	for(int i =0;i <= 44;i++){

		switch(spalte_search){

		case 1:

			if(strcmp(Tabelle[i].Kurzzeichen,phrase)== 0){
				not_found=0;
				Result_Lager[found]=Tabelle[i];
				found++;
			}
			break;
		case 2:

			if(strcmp(Tabelle[i].d,phrase)== 0){
				not_found=0;
				Result_Lager[found]=Tabelle[i];
				found++;
			}
			break;
		case 3:

			if(strcmp(Tabelle[i].D,phrase)== 0){
				not_found=0;
				Result_Lager[found]=Tabelle[i];
				found++;
			}
			break;
		case 4:

			if(strcmp(Tabelle[i].B,phrase)== 0){
				not_found=0;
				Result_Lager[found]=Tabelle[i];
				found++;
			}
			break;
		case 5:

			if(strcmp(Tabelle[i].C,phrase)== 0){
				not_found=0;
				Result_Lager[found]=Tabelle[i];
				found++;
			}
			break;
		case 6:

			if(strcmp(Tabelle[i].C_0,phrase)== 0){
				not_found=0;
				Result_Lager[found]=Tabelle[i];
				found++;
			}
			break;

		}
	}
	fclose(new_file);
}

void lifetime(double result[3],int size_t, double input[size_t], double p){

	result[0]= roundf((input[0]*input[1]+input[2]*input[3])*10)/10;

	result[1]= pow((input[4]/result[0]),p);

	result[2]= roundf(pow((input[4]/result[0]),p)*(pow(10,6)/(input[5]*60)));
}


double medium (double result[2][5]){

	double n_med=0;

	for(int i=0; i<= 4;i++){

		n_med= result[0][i]*(result[1][i]/100)+nm;
	}

	return n_med;
}


int get_criteria_life(HWND hwnd){

	int column = 0;

	radio_7 = IsDlgButtonChecked(hwnd,RB_7);
	radio_8 = IsDlgButtonChecked(hwnd,RB_8);

	while(column == 0){
		if(radio_7 == 1){
			column =1;
			break;
		}
		if(radio_8 == 1){
			column =2;
			break;
		}
	}
	return column;
}



